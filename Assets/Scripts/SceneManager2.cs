using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SceneManager2 : MonoBehaviour
{
    public static SceneManager2 instance;
    public GameObject optionsPanel;
    private bool activated;
    // Start is called before the first frame update
    void Start()
    {
        activated = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            activated = !activated;
        }

        if (activated)
        {
            optionsPanel.SetActive(true);
            Time.timeScale = 0f;
        }
        else
        {
            optionsPanel.SetActive(false);
            Time.timeScale = 1f;
        }

    }

}
