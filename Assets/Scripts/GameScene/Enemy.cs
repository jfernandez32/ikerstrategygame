using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    [Header("Sounds")]
    public AudioClip npcVoice;
    public AudioClip npcSound;

    [Header("Dialogues")]
    public GameObject dialoguePanel;
    public GameObject dialogueOmitir;
    public GameObject oxygenPanel;
    public GameObject inventoryPanel;
    public TMP_Text dialogueText;
    [TextArea(4, 6)] public string[] dialogueLines;

    //Variables privats
    private bool isPlayerinRange;
    private bool didDialogueStart;
    private int lineIndex;
    private float typingTime = 0.05f;
    private AudioSource TalkSound;

    private void Start()
    {
        TalkSound = GetComponent<AudioSource>();
        TalkSound.clip = npcVoice;
    }

    void Update()
    {
        if (isPlayerinRange)
        {
            if (!didDialogueStart)
            {
                TalkSound.PlayOneShot(npcSound);
                StartDialogue();
                dialogueOmitir.SetActive(true);
                oxygenPanel.SetActive(false);
                inventoryPanel.SetActive(false);
                Time.timeScale = 0.0f;
            }
            else if (dialogueText.text == dialogueLines[lineIndex])
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    NextDialogueLine();
                    Time.timeScale = 0f;
                }
            }
            else
            {
                StopAllCoroutines();
                dialogueText.text = dialogueLines[lineIndex];
            }
        }
        //Para omitir texto
        if(Input.GetKeyDown(KeyCode.C))
        {
            Omitir();
            TalkSound.Stop();
            dialogueOmitir.SetActive(false);
            oxygenPanel.SetActive(true);
            inventoryPanel.SetActive(true);
        }
    }

    private void StartDialogue()
    {
        didDialogueStart = true;
        dialoguePanel.SetActive(true);
        oxygenPanel.SetActive(false);
        inventoryPanel.SetActive(false);
        lineIndex = 0;
        Time.timeScale = 0f;
        StartCoroutine(ShowLine());
    }

    private void NextDialogueLine()
    {
        lineIndex++;
        if (lineIndex < dialogueLines.Length)
        {
            StartCoroutine(ShowLine());
        }
        else
        {
            dialogueOmitir.SetActive(false);
            didDialogueStart = false;
            dialoguePanel.SetActive(false);
            oxygenPanel.SetActive(true);
            inventoryPanel.SetActive(true);
            Time.timeScale = 1f;
            SceneManager.LoadScene("TutorialScene");
        }
    }

    private IEnumerator ShowLine()
    {
        dialogueText.text = string.Empty;

        foreach (char ch in dialogueLines[lineIndex])
        {
            dialogueText.text += ch;
            TalkSound.Play();
            yield return new WaitForSecondsRealtime(typingTime);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerinRange = true;
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerinRange = false;
        }

    }

     private void Omitir()
    {
            if(didDialogueStart)
            {
                //Salta al final del diálogo actual
                lineIndex = dialogueLines.Length -1;
                NextDialogueLine();
            }
    }

}
